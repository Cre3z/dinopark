import { Dinosaur, Activity, zoneMap } from '../types';

// init grid
export const zones: any = [];
export const allOurDinos: any = [];
export const dangerousDinos: any = [];
export const allTheThingsOurDinosDo: any = [];

// characters mapped = first column empty for row count
// corresponds with visual of columns on layout
export const columnNames = [
    '', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
];

// max row count
export const rowCount = 16;

// generate a row the length of the columns
for(let i = 0; i <= columnNames.length - 1; i++) {
    const row =[];
    for(let c = 0; c <= rowCount; c++) {
        row.push({ y: c, x: columnNames[i], value: `${columnNames[i] + c}` })
    }
    zones.push(row.reverse());
}

// zone index
const zoneIndex = (zone: Activity): { [key: string]: any } => {
    const loc = zone?.location;
    if(!loc) return {};
    const alpha = loc.replace(/[0-9]/g, '');
    const numeric = loc.replace(/\D/g,'');
    const hasColumn = columnNames.indexOf(alpha);
    return { col: hasColumn, i: numeric };
}

// lookup a zone in the grid
const hasZone = (zone: any): zoneMap | boolean =>{
    const { col, i } = zoneIndex(zone);
    if(!col || !i) return false;
    const lookUpZone = zones[col].reverse()[i];
    zones[col].reverse();
    return lookUpZone ? lookUpZone : false;
}

// check maintenance status >= 30 days
const checkMaintenanceStatus = (zone: any) => {
    var today: any = new Date(zone.time);
    var lastRecordedDate: any = new Date();
    const diffTime = Math.abs(lastRecordedDate - today);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays > 30;
}

const checkLocation = (id: string): [] => {
    // eslint-disable-next-line array-callback-return
    return allTheThingsOurDinosDo.filter((thing: Dinosaur | Activity) => {
        if("location" in thing && thing.location === id) return thing.location;
    });
}

const checkCarnivoreId = (id: string): boolean => {
    if(dangerousDinos.includes(id)) return true;
    return false;
}

// create zone updated version 
const metaZone = (zone: any, currentZone: any) => {
    const lookUpZone: any = {...currentZone};
    lookUpZone.meta = {...zone};
    // check maintenance
    if(zone.time) lookUpZone.meta.needsMaintenance = checkMaintenanceStatus(zone);
    const { value } = currentZone;
    // check last activity on the zone
    const lastActivity = checkLocation(value);
    // check danger status
    if(lastActivity.length) {
        const hasCarnivore = lastActivity.find((activity: any) => checkCarnivoreId(activity?.dinosaur_id));
        if(hasCarnivore) lookUpZone.meta.danger = true;
    }
    return lookUpZone;
}

// update a zone in the grid
export const updateZone = (zone: any) => {
    const zoneMeta = metaZone(zone, hasZone(zone));
    const { col, i } = zoneIndex(zone);
    zones[col].reverse()[i] = zoneMeta;
    zones[col].reverse();
}

// check if dino is herbivores
// const dangerousDinoList = ()

// updated dino list of last known location
export const updateDinoLists = (dino: any) => {
    if(dino.kind === "dino_added") allOurDinos.push(dino);
    if(dino.kind === "dino_added" && !("herbivore" in dino && dino.herbivore)) dangerousDinos.push(dino.id);
    allTheThingsOurDinosDo.push(dino);
}
