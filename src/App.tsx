import { FC, useEffect, useState } from 'react';

import './App.css';

import { ZoneGrid } from './components';

import { Activity, Dinosaur }  from './types';
import activityLog from './__mocks__/feed.json';

import logo from './assets/dinoparks-logo.png';

const App: FC = () => {

  const zoneLogs = useState<(Activity | Dinosaur)[]>([])

  useEffect(() => {
    // load data into the feed after DOM loads
    fetch('https://dinoparks.net/nudls/feed')
    .then(res => res.json())
    .then(data => 
      data.map((a: any) => zoneLogs.push(a))
    );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="App">
      
      <header>
        <img src={logo} alt="dinoparks" className="logo"/>
      </header>

      <ZoneGrid 
        feed={activityLog}
      />
    </div>
  );
}

export default App;

