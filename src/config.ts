type Config = {
    ApiResponseBodyCase: "camelize" | "underscored";
};
  
const config: Config = {
    ApiResponseBodyCase: "underscored",
};
  
export default config;
  