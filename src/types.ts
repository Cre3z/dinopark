export type zoneMap = {
    value: string;
    x: string;
    y: number;
    meta?: { [key: string]: any };
}

type gender = 'male' | 'female';

export interface Dinosaur {
    kind: string;
    name?: string;
    species?: string;
    gender?: gender;
    id?: number;
    digestion_period_in_hours?: number;
    herbivore?: boolean;
    park_id: 1;
    time: string;
    meta?: { [key: string]: any };
}

export interface Activity {
    kind: string;
    location?: string;
    dinosaur_id?: number;
    park_id: number;
    time: string;
    meta?: { [key: string]: any };
}