import { FC } from 'react';
import { zoneMap } from '../types';

import MaintenanceWrench from '../assets/dino-parks-wrench.png';

type InjectedProps = {
    meta: zoneMap;
};

const Zone: FC<InjectedProps> = ({ meta }) => {
    const zone = meta;

    const onlyNumbers = zone.value.match("^[0-9]+$") && zone.y !== 0;
    const onlyChars = zone.y === 0;
    
    let maintenace = zone?.meta?.needsMaintenance;
    let safe = !zone?.meta?.danger && maintenace;
    let danger = zone?.meta?.danger && maintenace;

    const maintenaceImg = <img src={MaintenanceWrench} alt="maintenance" className="maintenance--icon"/>;

    return (
        <div 
            data-zoneid={ zone.value } 
            className={
                `area-grid-zone ${safe ? "safe" : ""} 
                ${danger ? "danger" : ""}`
            }
            key={zone.value}
        >
            { maintenace && maintenaceImg }
            { onlyNumbers && zone.value}
            { onlyChars && zone.x}
        </div>
    );
}

export default Zone;