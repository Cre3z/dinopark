import { FC, useEffect, useState } from 'react';
import Zone from './Zone';
import './ZoneGrid.css';

import { zones, updateZone, updateDinoLists } from '../lib/gridHelpers';

import { Dinosaur, Activity, zoneMap } from '../types';

type InjectedProps = {
    feed: (Activity | Dinosaur)[];
};

const AreaGrid: FC<InjectedProps> = ({ feed }) => {

    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        feed.forEach((a: Activity | Dinosaur) => {
            if ("location" in a && a.kind === "maintenance_performed") {
                updateZone(a);
            } else {
                updateDinoLists(a);
            }
        });
        setLoading(false);
    }, [feed, loading]);
    
    const resultGrid = zones.map((col: any, i: any) => {
        return (
            <div className="area-grid-item" key={i}>
                {col.map((zone: zoneMap) => {
                    return (
                        <Zone meta={zone}/>
                    )
                })}
            </div>
        )
    });

    if(loading) return <div>Loading</div>;

    return (
        <div className="grid-container">
            <h1>Park Zones</h1>
            <div className="area-grid">
                { resultGrid }
            </div>
        </div>
    );
}

export default AreaGrid;