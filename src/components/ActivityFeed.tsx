import { FC } from 'react';

import AddActivity from './AddActivity';
// import { ActivityFeedStore } from "../stores/FeedStore";
import { Activity, Dinosaur } from '../types';

type InjectedProps = {
    feed: (Activity | Dinosaur)[];
};

const ActivityFeed: FC<InjectedProps> = ({ feed }) => {

    console.log(feed);
    

    return (
        <div className="activity-feed-container">
            
            <AddActivity />

            <ul>    
            {
                feed.map((a)  => {
                    return <li>{a.kind}</li>
                })
            }
            </ul>
        </div>
    );
}

export default ActivityFeed;